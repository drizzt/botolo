#!/usr/bin/ruby

require 'bundler/setup'
require 'multi_json'
require 'faye/websocket'
require 'eventmachine'
require 'em-http'

EM.run do
  wsOkCoin = Faye::WebSocket::Client.new('wss://real.okcoin.cn:10440/websocket/okcoinapi')
  httpHuobi = EM::HttpRequest.new('http://hq.huobi.com/socket.io/1/').get

  httpHuobi.callback do
    wsHuobi = Faye::WebSocket::Client.new("ws://hq.huobi.com/socket.io/1/websocket/#{httpHuobi.response.split(':').first}")
    wsHuobi.on :open do |event|
      p [:openHuobi]
#      wsHuobi.send("5:::#{MultiJson.dump({name: 'request', args: [{version: 1, msgType: 'reqSymbolList'}]})}")
      wsHuobi.send("5:::#{MultiJson.dump({name: 'request', args: [{version: 1, msgType: 'reqMsgSubscribe', symbolList: {lastTimeLine: [{symbolId: 'btccny', pushType: 'pushLong'}]}}]})}")
    end

    wsHuobi.on :message do |event|
      #p [:Huobi, event.data]
      if event.data.length > 3
        p [:Huobi, MultiJson.load(event.data[4..-1])]
      else
        wsHuobi.send("2::") if event.data == '2::'
        p [:Huobi, event.data]
      end
    end

    wsHuobi.on :close do |event|
      p [:closeHuobi, event.code, event.reason]
      wsHuobi = Faye::WebSocket::Client.new("ws://hq.huobi.com/socket.io/1/websocket/#{httpHuobi.response.split(':').first}")
      #wsHuobi = nil
    end
  end

  wsOkCoin.on :open do |event|
    p [:openOkCoin]
##    wsOkCoin.send(MultiJson.dump({event: 'addChannel', channel: 'ok_btccny_depth'}))
  end

  wsOkCoin.on :message do |event|
    p [:OkCoin, MultiJson.load(event.data)]
  end

  wsOkCoin.on :close do |event|
    p [:closeOkCoin, event.code, event.reason]
    wsOkCoin = Faye::WebSocket::Client.new('wss://real.okcoin.cn:10440/websocket/okcoinapi')
    #wsOkCoin = nil
  end
end
